#include <iostream>
#include "util.h"
#include "betterQuick.h"
#include "standardSortAlgorithms.h"

//-----------------------------------------------------------------------------
// ignores threads
void better_quick_sort_wrapper(int *A,int n,int threads) {
	// Currently just calls the standard quick_sort algorithm.
	// Replace this with your algorithm. It will probably 
	// be best to implement methods called better_quick_sort
	// and better_partition and have this algorithm call
	// better_quick_sort and have that use better_partition.
	 better_quick_sort(A,0,n-1);
}
void better_quick_sort(int *A, int l, int r) {
	if (r>l+16) {
	   int p=better_partition(A,l,r);
	   better_quick_sort(A,l,p-1);
	   better_quick_sort(A,p+1,r);
	}
	else {
		insertion_sort(A, l, r);
	}
}

int better_partition(int *A, int l, int r) {
   // Easiest way to avoid worst-case behavior
    Swap(A[l],A[(l+r)/2]);
    int p;
    p = A[l];
    int i = l+1;
    int j = r;
    while (1) {
        while (A[i] <= p && i < r) ++i;
        while (A[j] >= p && j > l) --j;
        if (i >= j) {
           Swap(A[j],A[l]);
           return j;
           }
        else {
             Swap(A[i],A[j]); 
             }
        }
}

/* 
Tried using Median of Three but it wasn't working :(

int better_partition(int *A, int l, int r) {
   // Easiest way to avoid worst-case behavior
    //Swap(A[l],A[(l+r)/2]);
    int p;
	int mid = (l+r)/2;
	
	// median of three pivot selection
	if (A[mid] < A[l]) {
		Swap(A[l],A[mid]);
	}
	if (A[r] < A[l]) {
		Swap(A[l],A[r]);
	}
	if (A[r] < A[mid]) {
		Swap(A[mid],A[r]);
	}
	Swap(A[l],A[mid]);
	p = A[l];
    int i = l+1;
    int j = r;
    while (1) {
        while (A[i] <= p && i < r) ++i;
        while (A[j] >= p && j > l) --j;
        if (i >= j) {
           Swap(A[j],A[l]);
           return j;
           }
        else {
            Swap(A[i],A[j]); 
        }
    }

}*/