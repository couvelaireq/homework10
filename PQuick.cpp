#include<omp.h>
#include<iostream>

#include "PQuick.h"
#include "standardSortAlgorithms.h"
#include "betterQuick.h"
#include "util.h"

//-------------------------------------------------------------------------
void p_quick_sort_wrapper(int *A,int n, int threads) {
	// Get the OMP stuff started and make an initial call
	// to your sorting algorithm, which I recommend you call
	// p_quick_sort
	
	// For now it just calls the better version of quick_sort
	
	#pragma omp parallel num_threads(threads)
	{
		//printf("Num Threads: %d", omp_get_num_threads());
		#pragma omp single
		{
			p_quick_sort(A,0,n-1);
		}
	}
}
//-------------------------------------------------------------------------

void p_quick_sort(int *A, int l, int r) {
	if (r>l+10000) {
	   int p=p_partition(A,l,r);
	   #pragma omp task
      p_quick_sort(A,l,p-1);
      #pragma omp task
      p_quick_sort(A,p+1,r);
	}
	else if (r>l+16) {
		int p=p_partition(A,l,r);
		p_quick_sort(A,l,p-1);
		p_quick_sort(A,p+1,r);
	}
	else {
		insertion_sort(A, l, r);
	}
}

int p_partition(int *A, int l, int r) {
   // Easiest way to avoid worst-case behavior
    Swap(A[l],A[(l+r)/2]);
    int p;
    p = A[l];
    int i = l+1;
    int j = r;
    while (1) {
        while (A[i] <= p && i < r) ++i;
        while (A[j] >= p && j > l) --j;
	if (i >= j) {
	   Swap(A[j],A[l]);
	   return j;
	   }
	else {
		 Swap(A[i],A[j]); 
		 }
	}
}
