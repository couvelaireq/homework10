#include "betterMerge.h"
#include "standardSortAlgorithms.h"

void better_merge_sort_wrapper(int *A,int n, int threads) { 
    // Currently just calls the standard merge_sort algorithm.
	// Replace this with your algorithm. It will probably 
	// be best to implement a method called better_merge_sort
	// that is your implementation and have this method call 
	// that method after doing any initialization of extra 
	// variables, etc. 
	// You will also need to implement better_merge.
    better_merge_sort(A,0,n-1);
}

void better_merge_sort(int *A, int l, int r) {
	
	if (r>l+16) {
		int mid = l+(r-l)/2;
        better_merge_sort(A, l, mid);
        better_merge_sort(A, mid + 1, r);
        better_merge(A, l, mid, r);
	}
	else if (r>l) {
		insertion_sort(A, l, r);
	}
}

void better_merge(int *A,int l,int m,int r) {
	int size=r-l+1; 
    int mid=m-l+1;
    int *B=new int[size];
    for(int i=0;i<mid;i++)
       B[i]=A[l+i];
    int blah=r+mid;
    for(int j=mid;j<size;j++)
       B[j]=A[blah-j];
    int i=0;
    int j=size-1;
    for(int k=l;k<=r;k++) {
       if(B[i]<B[j]) {
         A[k]=B[i];
         i++;
       }
       else {
         A[k]=B[j];
         j--;
       }
    }
    delete []B;
}