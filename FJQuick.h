#include <cstdlib>
// A node struct with a few more useful fields.
struct pnode {
     pnode* left;
     pnode* right;
     int sum; // the number of items in the range < threshold
     int sum2; // the number of items in the range > threshold
     int lo; // left endpoint, inclusive
     int hi; // right endpoint, exclusive
     int fl; // fromLeft for items < threshold
     int fl2; // fromLeft for items > threshold
     pnode() {
        left=0;
        right=0;
	lo=0;
	hi=0;
	sum=0;
	fl=0;
    	fl2=0;
	sum2=0;
     }
	pnode(int low,int high) {
		left=0;
		right=0;
		sum=0;
		fl=0;
    	fl2=0;
		sum2=0;
		lo=low;	
		hi=high;   
	}
	void print() { // for debugging
		std::cout<<"["<<lo<<", "<<hi<<")  "<<sum<<"   "<<fl<<"|"<<sum2<<"   "<<fl2
<<"\n";
	}
};
// Useful to return the range of pivot values in case there
// are more than one.  My partition 
struct Range {
	int lo;
	int hi;
};
void fj_quick_sort_wrapper(int *A,int n, int threads);
void fj_quick_sort(int *in, int *out, int l, int r);
int medianOfThree(int l, int m, int r);
void deleteTree(pnode* root);
void partitionUp(pnode* root, int in[], int thresh);
void partitionDown(pnode* root, int in[], int out[], int l, int l2, int 
thresh);
Range partitionP(int in[],int out[],int l, int r, int thresh);

