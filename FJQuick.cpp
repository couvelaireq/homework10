#include<omp.h>
#include <iostream>
#include <cstdlib>
#include<fstream>
#include "FJQuick.h"
#include "standardSortAlgorithms.h"
#include "betterQuick.h"
#include "util.h"
using namespace std;

void fj_quick_sort_wrapper(int *A,int n, int threads) {
	// Implement the fork-join parallel quick sort algorithm.
	// As with others, you will probably want to start some OMP stuff
	// here and then call an algorithm called fj_quick_sort.
	omp_set_num_threads(threads);
	//omp_set_dynamic(1);
    int* out = new int[n]; 
	#pragma omp parallel
	{
//  		#pragma omp for
//      for(int i=0;i<n;i++) {
//         out[i]=A[i];
//      }
        //printf("Num Threads: %d\n", omp_get_num_threads());
  		#pragma omp single nowait
  		{
        //printArray(A,n);
  			fj_quick_sort(A,out,0,n);
			
  		}
  }
  //printArray(A,n);
  //printArray(out,n);
  //printArray(A,n);
  delete[] out;    
  /* int* out = new int[n];
  int thresh = medianOfThree(A[0],A[n/2],A[n-1]);
  printf("Partition: %d\n",thresh);
  partitionP(A,out,0,n,thresh);
  printArray(out,n);
  thresh = medianOfThree(out[0],out[(n-2)/2],out[n-2-1]);
  printf("Partition: %d\n",thresh);
  partitionP(out,A,0,n-2,thresh);
  printArray(A,n);
  //printArray(out,n);
  thresh = medianOfThree(out[n-2],out[n-2],out[n-1]);
  printf("Partition: %d\n",thresh);
  partitionP(out,A,n-2,n,thresh);
  printArray(A,n); */
}
//-------------------------------------------------------------------------

void fj_quick_sort(int *in, int *out, int l, int r) {
	if (r-l > 10000) {
		//int size = r-l;
		int mid = l+(r-l)/2;
		//printf("l: %d, r: %d, mid: %d\n",l,r,mid);
		int thresh = medianOfThree(in[l],in[mid],in[r-1]);
		//printf("thresh: %d\n",thresh);
		Range range = partitionP(in,out,l,r,thresh); // in is unpartitioned array
                                                // out is the resulting partitioned
                                                // array 
        for (int i = range.lo; i<range.hi;i++) {
			in[i] = out[i];
		}			
	// for(int i = l; i < r;i++) 
	// {printf("in[%d]: %d out[%d]: %d\n",i,in[i],i,out[i]);}
	//printf("Add of inA: %p  Add of other: %p", inA, &valOfInA);
	#pragma omp task untied //firstprivate(out,in,l,range)
    fj_quick_sort(out,in,l,range.lo);
    
    //printf("%d %d\n",range.hi,r);
	#pragma omp task untied //firstprivate(out,in,r,range)
	  fj_quick_sort(out,in,range.hi,r);
	  //#pragma omp taskwait
	}
	else if (r-l > 16) {
		//int size = r-l;
		int mid = l+(r-l)/2;
		//printf("l: %d, r: %d, mid: %d\n",l,r,mid);
		int thresh = medianOfThree(in[l],in[mid],in[r-1]);
		//printf("thresh: %d\n",thresh);
		Range range = partitionP(in,out,l,r,thresh); // in is unpartitioned array
                                                // out is the resulting partitioned
                                                // array 
        for (int i = range.lo; i<range.hi;i++) {
			in[i] = out[i];
		}			
	// for(int i = l; i < r;i++) 
	// {printf("in[%d]: %d out[%d]: %d\n",i,in[i],i,out[i]);}
	//printf("Add of inA: %p  Add of other: %p", inA, &valOfInA);
		  //printf("%d %d\n",1, range.lo);
      fj_quick_sort(out,in,l,range.lo);
    //printf("%d %d\n",range.hi,r);
	  fj_quick_sort(out,in,range.hi,r);
	}
	else {
    //printf("Base case: %d: %d\n",l,in[l]);
    insertion_sort(in, l, r-1);
	for(int i = l; i < r; i++){
		out[i]=in[i];
		//printf("Threads: %d", omp_get_num_threads());
		//printf("base case: out[%d]: %d\n",i,out[i]);
	}
	
	}
}
int medianOfThree(int l, int m, int r) {
	if (m < l) {
		Swap(m,l);
	}
	if (r < l) {
		Swap(l,r);
	}
	if (r < m) {
		Swap(m,r);
	}
	return m;
	
}

//---------------------------------------------------------------------
// Partition
// 
// Here are the function prototypes for my partitionP algorithm
// that does partition of an array in parallel using one up and one
// down pass.  It is a bit complicated, so feel free to ignore this
// and instead implement a packHigh algorithm and use pack and packHigh
// to implement partition.



// A useful method to call at the end of your algorithm to delete the tree.
void deleteTree(pnode* root) {
	if(root!=0) {
		#pragma omp task untied
		deleteTree(root->left);
		deleteTree(root->right);
		#pragma omp taskwait
		delete root;
	}
}


void partitionUp(pnode* root, int in[], int thresh) {
	//implement me (or not)
	static const int SEQUENTIAL_CUTOFF = 20000;
	int lo=root->lo;
	int hi=root->hi;
	// Use a sequential cutoff to do the work in 
	// serial when it is small enough
	if (hi-lo <= SEQUENTIAL_CUTOFF) {
		root->sum=0;
		root->sum2=0;
		for (int i = lo; i < hi; i++) {
			if(in[i]<thresh) root->sum++;
			if (in[i]>thresh) root->sum2++;
		}
	} else {
		int mid=lo+(hi-lo)/2;
                pnode* left = new pnode(lo,mid);
  		pnode* right = new pnode(mid,hi); 
		root->left=left;
		root->right=right;
		// Run first part on another thread
		#pragma omp task untied //firstprivate(left,in,thresh)
		partitionUp(left,in,thresh);
		// Run second part on this thread
		//#pragma omp task untied //firstprivate(right,in,thresh)
		partitionUp(right,in,thresh);
		#pragma omp taskwait
		root->sum = left->sum+right->sum;
		root->sum2 = left->sum2+right->sum2;
	}
}

// out is where the results of partition should go.
// l is the left endpoint of this call to partitionDown.
// l2 is the left endpoint of where the "higher" elements go.
// ****Both l and l2 should be passed as-is in recursive calls. 
// thresh is the pivot value.
//
void partitionDown(pnode* root, int in[], int out[], int l, int l2, int 
thresh) {
	// implement me (or not)
	pnode* left = root->left;
	if(left!=0) {
    
		left->fl = root->fl;
		root->right->fl = root->fl + left->sum;
		left->fl2 = root->fl2;
		root->right->fl2 = root->fl2 + left->sum2;
		// Run first part on another thread
		#pragma omp task untied //firstprivate(left,in,out,l,l2,thresh)
		partitionDown(left,in,out,l,l2,thresh);
		// Run second part on this thread
		//#pragma omp task untied //firstprivate(root,in,out,l,l2,thresh)
		partitionDown(root->right,in,out,l,l2,thresh);
		#pragma omp taskwait
	} else {
		// The sequential-cutoff part of the down pass is
		// determined by the nodes that have already been
		// constructed.  Since root has no children, we
		// are at the cutoff and proceed sequentially.
		int lo=root->lo;
		int hi=root->hi;
		int j = l+root->fl;
		int j2 = l2+root->fl2;
		for(int i=lo;i<hi;i++) {
			if(in[i]<thresh) {
				out[j]=in[i];
				j++;
			}
			if(in[i]>thresh) {
				out[j2]=in[i];
				j2++;
			}
		}
	}
}

// Partition array in into array out from l (inclusive) to r (exclusive) using
// thresh as the pivot value.
// Return the range of pivot values (inclusive of lo, exclusive of hi).
// That is, the range of indices where the value thresh ended up (in case 
// there
// are more than one).
Range partitionP(int in[], int out[], int l, int r, int thresh) {
	// Implement me (or not)
	//omp_set_num_threads(threads);
	//omp_set_nested(1);
	pnode *root;
	int index_for_lo,l2;
	//#pragma omp parallel
	{
		//printf("Num Threads2: %d\n", omp_get_num_threads());
	//#pragma omp single
	{
		//printf("Num Threads2: %d\n", omp_get_num_threads());
		root = new pnode(l,r);
		partitionUp(root,in,thresh);
		int num_of_pivots = r-l - (root->sum + root->sum2); // len of subarray minus (total num elements less than thresh plus total num elements more than thresh)
		index_for_lo = l + root->sum;
		l2 = num_of_pivots + index_for_lo; // index of left endpoint of "higher" elements
		partitionDown(root,in,out,l,l2,thresh);
		// fill in the pivots      
		for(int i = index_for_lo; i < l2; i++) {
			out[i]=thresh;
		}	
	}
	}
   
	Range range = Range();
	range.lo = index_for_lo;
	range.hi = l2;
    //printf("lo: %d, hi: %d \n",	range.lo, range.hi);
  //#pragma omp barrier
	deleteTree(root);
	return range; // so it compiles and works hopefully.
}

